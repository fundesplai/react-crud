import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";

import { Button, Row, Col, FormGroup, Label, Input } from "reactstrap";
import Controller from './ContactesController';

import axios from "axios";

// anterior xmysql
// xmysql -h localhost -d agenda -u root

const API = "http://localhost:3003";

const EditaFotoContacte = (props) => {

  const [tornar, setTornar] = useState(false);
  const [selectedFile, setSelectedFile] = useState(false);
  const [contacte, setContacte] = useState(false);

  useEffect(()=>{
    const id = props.match.params.id;
    Controller.getById(id)
    .then(data => setContacte(data))
    .catch(err => {
      return <Redirect to="/contactos" />;
    });
  }, [])

  const enviarFoto = () => {
    const data = new FormData();
    data.append("file", selectedFile);
    data.append("idcontacto", contacte.id);
    axios.post(API + "/contactos/foto", data).then((res) => {
      console.log(res.statusText);
      setTornar(true);
    });
  };

  if (tornar) {
    return <Redirect to="/contactos" />;
  }

  return ( 
    <>
      <br />
      <Row>
        <Col>
          <h1>Foto para: {contacte && contacte.nombre  } </h1>
        </Col>
        <Col>
          <span className="float-right">
            <Button
              type="button"
              onClick={() => setTornar(true)}
              size="sm"
              color="danger"
            >
              {"Sortir sense desar"}
            </Button>
            {' '}
            <Button onClick={enviarFoto} size="sm" color="success">
              {"Desar canvis"}
            </Button>
          </span>
        </Col>
      </Row>
      <br />

      <Row>
        <Col sm="6">
          <FormGroup>
            <Label for="nombreFoto">Nom</Label>
            <Input
              type="file"
              onChange={(e) => setSelectedFile(e.target.files[0])}
            />
          </FormGroup>
        </Col>
      </Row>
    </>
  );
};

export default EditaFotoContacte;
