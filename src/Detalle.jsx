import React, {useContext, useState, useEffect} from "react";
import {Redirect, Link} from "react-router-dom";
import Controller from './ContactesController';

import TraductorContext from "./TraductorContext.js";

export default (props) => {
  const Traductor = useContext(TraductorContext);

  const [contacto, setContacto] = useState({});


  useEffect(()=>{
    const id = props.match.params.id;
    Controller.getById(id)
    .then(data => setContacto(data))
    .catch(err => {
      return <Redirect to="/contactos" />;
    });
  }, [])


  return (
    <>
      <h3>{Traductor.traduce('detalle')}</h3>
      <hr />
      <h1>{Traductor.traduce('nombre')}: {contacto.nombre}</h1>
      <h3>{Traductor.traduce('email')}: {contacto.email}</h3>
      <h3>{Traductor.traduce('tel')}: {contacto.tel}</h3>
      <img src={'http://localhost:3003/img/'+contacto.urlfoto} width="200"/>
      <hr />
      <Link className='btn btn-primary' to='/contactos' >{Traductor.traduce('volver')}</Link>
    </>
  );
};
